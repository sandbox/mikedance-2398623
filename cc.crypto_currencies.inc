<?php
/**
 * @file
 * Provides crypto currency helper function and hooks.
 */

/**
 * Returns an array of crypto currencies.
 */
function cc_crypto_currencies() {
  $output = module_invoke_all('cc_crypto_currencies');

  return $output;
}

/**
 * Returns an array of crypto currencies suitable for form api options.
 */
function cc_crypto_currency_options() {
  $output = array();

  $results = cc_crypto_currencies();

  if (is_array($results)) {
    foreach ($results as $key => $value) {
      $output[$key] = $value['title'];
    }
  }

  return $output;
}

/**
 * Returns an array of enabled cryto currencies.
 */
function cc_crypto_currencies_enabled() {
  $default = array();

  $output = variable_get('cc_crypto_currencies_enabled', $default);

  return $output;
}
