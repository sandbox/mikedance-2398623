<?php
/**
 * @file
 * Provides network type helper function and hooks.
 */

/**
 * Provides the main network type string.
 */
define('CC_NETWORK_TYPE_MAIN', 'main');

/**
 * Provides the test network type string.
 */
define('CC_NETWORK_TYPE_TEST', 'testnet');

/**
 * Provides the regtest network type string.
 */
define('CC_NETWORK_TYPE_REG', 'regtest');

/**
 * Returns a sting constant containing the default network type.
 */
define('CC_NETWORK_TYPE', CC_NETWORK_TYPE_TEST);

/**
 * Returns an array of crypto currency network types.
 */
function cc_network_types() {
  $output = module_invoke_all('cc_network_types');

  return $output;
}

/**
 * Implements hook_cc_network_types().
 */
function cc_cc_network_types() {
  $output = array();

  $output[CC_NETWORK_TYPE_MAIN] = t('Main');
  $output[CC_NETWORK_TYPE_TEST] = t('Test');
  $output[CC_NETWORK_TYPE_REG] = t('Reg Test');

  return $output;
}

/**
 * Returns a string constant containing the active network type.
 */
function cc_network_type() {
  $output = variable_get('cc_network_type', CC_NETWORK_TYPE);

  return $output;
}
