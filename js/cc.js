/**
 * @file
 * Provides the crypto currency javascript functionality.
 */

/*jslint vars: false, white: true, indent: 2 */
/*global window, document, jQuery, Drupal */
(function ($) {
  "use strict";

  var namespace,
    methods;

  namespace = 'cc';

  $(document).ready(
    function() {
    }
  );

  Drupal.behaviors[namespace] = {
    attach: function (context, settings) {
      $(context).cc(settings);
    }
  };

  methods = {};

  methods.init = function (settings) {
    var $this;

    $this = $(this);

    return $this;
  };

  $.fn[namespace] = function (method) {
    var name, message;

    if (methods[method]) {
      name = Array.prototype.slice.call(arguments, 1);

      return methods[method].apply(this, name);
    }
    else if (typeof method === 'object' || !method) {
      return methods.init.apply(this, arguments);
    } else {
      message = 'Method ' + method + ' does not exist on jQuery.' + namespace;

      $.error(message);
    }
  };
})(jQuery);
