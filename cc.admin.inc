<?php
/**
 * @file
 * Provides administration functionality.
 */

/**
 * Provides the admin settings form.
 */
function cc_admin_settings() {
  $form = array();

  $description = t('Please specify the active network type.');
  $options = cc_network_types();
  $default_value = cc_network_type();

  $form['cc_network_type'] = array(
    '#type' => 'radios',
    '#title' => t('Active Network Type'),
    '#description' => $description,
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $description = t('Please specify the crypto currencies which should be enabled for the site.');
  $options = cc_crypto_currency_options();
  $default_value = cc_crypto_currencies_enabled();

  $form['cc_crypto_currencies_enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled Crypto Currencies'),
    '#description' => $description,
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $options = cc_network_options();
  $default_value = cc_networks_enabled();

  $form['cc_networks_enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled Networks'),
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $options = cc_js_type_options();
  $default_value = cc_js_type();

  $form['cc_js_type'] = array(
    '#type' => 'radios',
    '#title' => t('Javascript Type'),
    '#options' => $options,
    '#default_value' => $default_value,
  );

  return system_settings_form($form);
}
