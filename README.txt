CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
This project provides a crypto currency framework to make integrating crypto
currencies into your drupal site simpler.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/cc

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/cc

Here is a roadmap for this project:

Crypto Currency library

  The Crypto Currency library will provide a generic namespace, classes,
interfaces, and abstractions common to all crypto currencies.  The library
will contain only PHP code so that it is portable throughout different systems
and platforms.

  Crypto currencies can provide a namespace for Bitcoin, Litecoin, Namecoin,
etc. to integrate any specific functionality or features.  I am currently
evaluating https://github.com/afk11/bitcoin as a foundation for this library.

Cryto Currency module suite

  The Crypto Currency module suite is a collection of modules providing the
drupal related functionality, integration, and wrappers with the Crypto Currency
library.

  Here is an overview of the proposed module structure and hierarchy:

- cc: Provides the main crypto currency foundation, api,
administration section, etc.

- cc_btc: Provides the bitcoin crypto currency integration,
CryptoCurrency Bitcoin library wrappers, bitcoinjs, etc.
- cc_{nmc, ltc, doge}: Provides the alt coin crypto currency integration

- cc_gateways: Provides the gateway functionality, classes, api, and
connection handling to the crypto currency network
  - cc_btc_gateway_core:  Provides the bitcoin core client gateway,
connection settings, etc.
  - cc_btc_gateway_{bc,insight,bitcoinj,blocktrail,etc.}: Provides the
{blockchain.info, insight, bitcoinj, blocktrail, etc.} gateway,
administration settings form, api keys, etc.

- cc_addresses: Provides the address functionality, api, and field
subsystem plugins
  - cc_btc_address_core: Provides addresses sourced by the bitcoin core client
  - cc_btc_address_hd: Provides addresses sourced by hd pub keys
  - cc_btc_address_bc: Provides addresses sourced by a blockchain.info wallet

- cc_amount: Provides the crypto currency amount functionality, and
field subsystem plugins

- cc_currencies: Provides the fiat currencies functionality, and api,
which leverages the currency module
(https://www.drupal.org/project/currency)

REQUIREMENTS
------------
There are no requirements for this module at this time.

RECOMMENDED MODULES
-------------------
There are no recommended modules at this time.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

 The Administer Crypto Currencies is used to provide global administrative
 access to the crypto currency module suite.

 * Configure any crypto currency related settings under the Crypto Currencies
 administration section at http://yoursite.com/admin/cc

TROUBLESHOOTING
---------------
There are no troubleshooting recommendations at this time.

FAQ
---
There are no frequently asked questions at this time.

MAINTAINERS
-----------
Current maintainers:
 * Michael Dance (mikedance) - https://www.drupal.org/u/mikedance
