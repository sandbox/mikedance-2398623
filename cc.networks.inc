<?php
/**
 * @file
 * Provides network helper function and hooks.
 */

/**
 * Returns an array of crypto currency networks.
 *
 * @param string $type
 *    A CC_NETWORK_TYPE constant to specify the type of network to return.  If
 *    not specified, all networks will be returned.
 */
function cc_networks($type = NULL) {
  $output = array();

  $networks = module_invoke_all('cc_networks');

  if (is_null($type)) {
    $output = $networks;
  }
  else {
    foreach ($networks as $network) {
      if ($network['type'] == $type) {
        $output[$network['machine_name']] = $network;
      }
    }
  }

  return $output;
}

/**
 * Returns an array of crypto currency networks suitable for form api options.
 *
 * @param string $type
 *    A CC_NETWORK_TYPE constant to specify the type of network to return.  If
 *    not specified, all networks will be returned.
 */
function cc_network_options($type = NULL) {
  $output = array();

  $networks = cc_networks($type);

  if (is_array($networks)) {
    foreach ($networks as $network) {
      $output[$network['machine_name']] = $network['title'];
    }
  }

  return $output;
}

/**
 * Returns an array of enabled cryto currency networks.
 */
function cc_networks_enabled() {
  $default = array();

  $output = variable_get('cc_networks_enabled', $default);

  return $output;
}
