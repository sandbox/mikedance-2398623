<?php
/**
 * @file
 * Provides crypto currency functionality.
 */

require_once 'cc.helpers.inc';
require_once 'cc.crypto_currencies.inc';
require_once 'cc.network_types.inc';
require_once 'cc.networks.inc';

/**
 * Implements hook_permission().
 */
function cc_permission() {
  $output = array();

  $output[CC_ADMIN_PERMISSION] = array(
    'title' => t('Administer Crypto Currencies'),
    'description' => t('Allows users to administer crypto currency functionality.'),
  );

  return $output;
}

/**
 * Implements hook_menu().
 */
function cc_menu() {
  $output = array();

  $output[CC_ADMIN_PREFIX] = array(
    'title' => 'Crypto Currencies',
    'page callback' => 'cc_admin_page',
    'access arguments' => array(CC_ADMIN_PERMISSION),
    'file' => 'cc.admin.inc',
    'weight' => 10,
  );

  $output[CC_ADMIN_PREFIX . '/global'] = array(
    'title' => 'Global',
    'description' => 'Configure global settings.',
    'position' => 'left',
    'weight' => -50,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array(CC_ADMIN_PERMISSION),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $output[CC_ADMIN_PREFIX . '/global/settings'] = array(
    'title' => 'Settings',
    'description' => 'Configure crypto currency settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cc_admin_settings'),
    'access arguments' => array(CC_ADMIN_PERMISSION),
    'file' => 'cc.admin.inc',
    'weight' => -50,
  );

  return $output;
}

/**
 * Provides the admin section.
 */
function cc_admin_page() {
  $blocks = array();

  $sql = <<<EOF
SELECT
menu_name,
mlid
FROM
{menu_links}
WHERE
link_path = :link_path AND
module = 'system'
EOF;

  $args = array(
    ':link_path' => CC_ADMIN_PREFIX,
  );

  if ($admin = db_query($sql, $args)->fetchAssoc()) {
    $result = db_query("
      SELECT m.*, ml.*
      FROM {menu_links} ml
      INNER JOIN {menu_router} m ON ml.router_path = m.path
      WHERE ml.link_path <> 'admin/help' AND menu_name = :menu_name AND ml.plid = :mlid AND hidden = 0", $admin, array('fetch' => PDO::FETCH_ASSOC));
    foreach ($result as $item) {
      _menu_link_translate($item);
      if (!$item['access']) {
        continue;
      }
      // The link description, either derived from 'description' in hook_menu()
      // or customized via menu module is used as title attribute.
      if (!empty($item['localized_options']['attributes']['title'])) {
        $item['description'] = $item['localized_options']['attributes']['title'];
        unset($item['localized_options']['attributes']['title']);
      }
      $block = $item;
      $block['content'] = '';
      $block['content'] .= theme('admin_block_content', array('content' => system_admin_menu_block($item)));
      if (!empty($block['content'])) {
        $block['show'] = TRUE;
      }

      // Prepare for sorting as in function _menu_tree_check_access().
      // The weight is offset so it is always positive, with a uniform 5-digits.
      $blocks[(50000 + $item['weight']) . ' ' . $item['title'] . ' ' . $item['mlid']] = $block;
    }
  }

  if ($blocks) {
    ksort($blocks);
    return theme('admin_page', array('blocks' => $blocks));
  }
  else {
    return t('You do not have any administrative items.');
  }
}
