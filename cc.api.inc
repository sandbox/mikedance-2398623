<?php
/**
 * @file
 * Provides API and hook documentation.
 */

/**
 * Implements hook_cc_network_types().
 *
 * Responsible for defining crypto currency network types.
 */
function hook_cc_network_types() {
  $output = array();

  $output[CC_NETWORK_TYPE_MAIN] = t('Main');
  $output[CC_NETWORK_TYPE_TEST] = t('Test');
  $output[CC_NETWORK_TYPE_REG] = t('Reg Test');

  return $output;
}

/**
 * Implements hook_cc_networks().
 *
 * Responsible for defining a crypto currency networks settings.
 */
function hook_cc_networks() {
  $output = array();

  $output['btc'] = array(
    'machine_name' => 'btc',
    'type' => CC_NETWORK_TYPE_MAIN,
    'title' => t('Bitcoin'),
    'links' => array(
      'explorer' => 'http://blockchain.info/address/',
    ),
  );

  $output['btc_test'] = array(
    'machine_name' => 'btc_test',
    'type' => CC_NETWORK_TYPE_TEST,
    'title' => t('Bitcoin Testnet'),
    'links' => array(
      'explorer' => 'http://blockexplorer.com/testnet/address',
    ),
  );

  $output['btc_reg'] = array(
    'machine_name' => 'btc_reg',
    'type' => CC_NETWORK_TYPE_REG,
    'title' => t('Bitcoin Regtest'),
    'links' => array(),
  );

  return $output;
}

/**
 * Implements hook_cc_address_types().
 */
function hook_cc_address_types() {
  $output = array();

  $description = t('Use the bitcoin core client for address generation.');

  $output['cc_btc_address_type_core'] = array(
    'title' => t('Bitcoin Core Address Generation'),
    'description' => $description,
    'class' => '\CryptoCurrency\Bitcoin\AddressCore',
  );

  return $output;
}
