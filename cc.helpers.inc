<?php
/**
 * @file
 * Provides helper functions.
 */

/**
 * Provides a string containing the default menu prefix.
 */
define('CC_ADMIN_PREFIX', 'admin/cc');

/**
 * Provides an integer representing the url segment count for the menu prefix.
 *
 * This should be 0 based as it will be used for menu load calculations.
 */
define('CC_ADMIN_PREFIX_COUNT', 1);

/**
 * Provides a string representing the default administrative permission.
 */
define('CC_ADMIN_PERMISSION', 'administer cc');

/**
 * Returns a string containing the production javascript include type.
 */
define('CC_JS_TYPE_PROD', 'prod');

/**
 * Returns a string containing the development javascript include type.
 */
define('CC_JS_TYPE_DEV', 'dev');

/**
 * A helper function for adding crypto currency related javascript libraries.
 *
 * This function relies on the drupal_add_js() function to include
 * the libraries.
 *
 * @param array $input
 *    An array of javascript library keys to include on the page.
 *
 * @return array
 *    An array of javascript and css library files which have been passed
 *    through drupal_add_js() and drupal_add_css() for inclusion on the page.
 */
function cc_add_js($input = array('all')) {
  $output = array(
    'js' => array(),
    'css' => array(),
  );

  if (is_string($input)) {
    $input = array($input);
  }

  $input[] = 'cc';

  $path = drupal_get_path('module', 'cc');

  $js = array(
    'cc' => array(
      $path . '/js/cc.js',
    ),
  );

  foreach ($input as $key) {
    if ($key == 'all' || isset($js[$key])) {
      $files = $js[$key];

      foreach ($files as $file) {
        drupal_add_js($file);
      }
    }
  }

  $output['js'] = $js;

  $css = array();

  foreach ($input as $key) {
    if ($key == 'all' || isset($css[$key])) {
      $files = $css[$key];

      foreach ($files as $file) {
        drupal_add_css($file);
      }
    }
  }

  $output['css'] = $css;

  return $output;
}

/**
 * Returns an array of javascript type options.
 */
function cc_js_type_options() {
  $output = array(
    'dev' => t('Development'),
    'prod' => t('Production'),
  );

  return $output;
}

/**
 * Returns a string containing the type of javascript file to use.
 */
function cc_js_type() {
  $output = variable_get('cc_js_type', CC_JS_TYPE_PROD);

  return $output;
}
